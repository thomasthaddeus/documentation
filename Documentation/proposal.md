# Dependency Scanners Proposal

> [[__TOC__]]

## FORWARD

This document intends to be a Guide to the different dependency scanners that can be applied to <strong>LibreFood Pantry open-source project</strong>. Under each section, we can find a quick explanation of how to use and the functionalities of each dependency canners  available in GitLab.

### TERMS

1. **SAST** - Static Analysis
2. **SCA** - Software Composition Analysis
3. **SBOM** - software bill of materials
4. **XSS** - cross-site scripting


## Security scanners with auto-DevOps configuration

We can add various security scanners, customize the `.gitlab-ci.yml` and add pre-defined templates according to the area we want to look for vulnerability. In the following section we will define the different security scenes available in GitLab and the area of action:

### Static Application Security Testing (SAST)

**SAST** is available in GitLab at all tiers. It can be added to any project with a `YML` file using the following format: 

   ```yaml
   - template: Security/SAST.gitlab-ci.yml
   ```

**SAST**, rust static analysis over the source code to check for known vulnerabilities, after running the scanners **SAST** reports the finding in `JSON`.
   - ![Missing Picture](Picture)

Format and sort ascending by priority. E.g.
   1. Critical
   2. High
   3. Medium 
   4. Low
   5. Info
   6. Unknown

**SAST** can detect unsafe code potentially leading to dangerous code execution such as **XSS**. \
One disadvantage of running **SAST** is that it requires a `Linux/amd64` container, and it does not support Windows containers. 

### Dependency Scanner

   ```yaml
   - template: Security/Dependency-Scanning.gitlab-ci.yml
   ```

_Dependency scanners_ scan your code for dependency vulnerabilities and rapport and open-source libraries that may contain malicious code. (GitLab. 20202) "Dependency Scanning is often considered part of **SCA**. **SCA** can contain aspects of inspecting the items your code uses."

### Licensing Scanner

In GitLab, we can configure scanners to ensure that our code does not infringe with any license agreement.

Configured dependency scanners allow developers to detect security vulnerabilities and make sure the project meets licensing requirements. \
This is important to avoid legal litigation. We can add licensing scanners by adding this scanner in the `YML` file and adding the pre-defined template as follows:

   ```yaml
   - template: Security/License-Scanning.gitlab-ci.yml
   ```

"For the job to activate, **License Finder** needs to find a compatible package definition in the project directory. For details, see the Activation on License Finder documentation. GitLab checks the License Compliance report, compares the licenses between the source and target branches, and shows the information right on the merge request. Denied licenses are indicated by an x-red icon next to them as well as new licenses that need a decision from you. In addition, you can manually allow or deny licenses in your project's license compliance policy section."

### Container Scanner – Grype

    ```yaml
    - template: Security/Container-Scanning.gitlab-ci.yml
    ```

**Grype** is a security scanner for container vulnerabilities to find known treats and scan for flaws in the file systems. According to (GitLab, 2022), **Grype** is "A vulnerability scanner for container images and file systems. Easily install the binary to try it out. Works with **Syft**, the powerful _SBOM_ tool for container images and filesystems."

#### Grype Capabilities

1. Grype can scan vulnerabilities for common OS packages such as 
    - Alpine
    - Amazon Linux
    - BusyBox
    - CentOS
    - Debian
    - Distress
    - Oracle Linux
    - Red Hat (RHEL)
    - Ubuntu
1. Also, for specific packages: 
    - Ruby (Gems)
    - Java (JAR, WAR, EAR, JPI, HPI)
    - JavaScript (NPM, Yarn)
    - Python (Egg, Wheel, Poetry, requirements.txt/setup.py files)
    - Dotnet (deps.json)
    - Golang (go.mod)
    - PHP (Composer)
    - Rust (Cargo)
