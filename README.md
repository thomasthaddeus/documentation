# Review Apps

The document is for end users. If you will be maintaining Review Apps, please start by reading [Handoff-August-2021.md](Handoff-August-2021.md).

> [[_TOC_]]

## 1. Overview

These instructions will enable GitLab's [Review Apps](https://docs.gitlab.com/ee/ci/review_apps/) (via GitLab's [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/) and [Auto Review Apps](https://docs.gitlab.com/ee/topics/autodevops/stages.html#auto-review-apps)) to deploy to [Amazon Web Services (AWS)](https://aws.amazon.com/) using its [Elastic Container Service (ECS)](https://aws.amazon.com/ecs/) backed by [Fargate](https://aws.amazon.com/fargate/). To reduce costs, this setup will also automatically stop a deployed review app within 15-75 minutes after it was started.

Each GitLab project so configured will be able to deploy a single review app at a time. That means that each commit to the project's repository will automatically deploy a review app for that commit, overwriting any existing review app previously deployed for the project. In the future we would like to allow multiple review apps per project (e.g., one per commit, one per branch, etc.).

The setup described allows review apps to access the credentials necessary to manipulate AWS ECS without developers (Shop Members) knowing the values of those credentials.

### 1.1. Relationship between GitLab and AWS

Each project in GitLab deploys to a unique [ECS Cluster](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/clusters.html).

```plantuml
@startuml
cloud GitLab {
    frame Project1
    frame Project2
}

cloud AWS {
    frame Cluster1
    frame Cluster2
}
Project1 --> Cluster1 : Deploys to
Project2 --> Cluster2 : Deploys to
@enduml
```

**_Figure 1: GitLab Projects have a 1-to-1 relationship with AWS Clusters._**

### 1.2. Variables and Permissions

In an effort to control who has access to the variables' values, some variables are defined with the project itself while more sensitive variable are defined in the main LibreFoodPantry project and are inherited into the projects.

The sensitive variables that must be defined at a higher level are the following:

- `AWS_ACCESS_KEY_ID`
- `AWS_DEFAULT_REGION`
- `AWS_SECRET_ACCESS_KEY`

Only one account is used for all of LFP. So these values are set in the LFP group. All projects can access them, but developers will not be able to view their values.

The non-sensitive variables that are set at the project level are the following:

- `CI_AWS_ECS_CLUSTER`
- `CI_AWS_ECS_SERVICE`
- `CI_AWS_ECS_TASK_DEFINITION`

All of the `CI-AWS_ECS_*` [variables must be set](https://docs.gitlab.com/ee/ci/cloud_deployment/index.html#deploy-your-application-to-the-aws-elastic-container-service-ecs) to the names of the AWS artifacts that were created specifically for use by this project.

```plantuml
@startuml
storage "LFP CI/CD Variables" as lfpvars {
    artifact "AWS_ACCESS_KEY_ID=***\nAWS_DEFAULT_REGION=***\nAWS_SECRET_ACCESS_KEY=***"
}
storage "Project1 CI/CD Variables" as Project1Vars {
    artifact "AUTO_DEVOPS_PLATFORM_TARGET=FARGATE\nCI_AWS_ECS_CLUSTER=...\nCI_AWS_ECS_SERVICE=...\nCI_AWS_ECS_TASK_DEFINITION=..." as vars1
}
storage "Project2 CI/CD Variables" as Project2Vars {
    artifact "AUTO_DEVOPS_PLATFORM_TARGET=FARGATE\nCI_AWS_ECS_CLUSTER=...\nCI_AWS_ECS_SERVICE=...\nCI_AWS_ECS_TASK_DEFINITION=..." as vars2
}
@enduml
```

**_Figure 2: Where to define CI/CD variables in GitLab_**

### 1.3. Components and Structure of GitLab and AWS

This diagram shows the components and structure of GitLab and AWS. The CI/CD project variables are described in the previous section. The GitLab repository contains a [`.gitlab-ci.yml`](https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html) file and [`Dockerfile`](https://docs.docker.com/engine/reference/builder/). The `.gitlab-ci.yml` file, whose contents will be given later, activates Auto DevOps. Auto DevOps will use the `Dockerfile` to build the image and store it in the [container registry](https://docs.gitlab.com/ee/user/packages/container_registry/).

In AWS, there is a [Cluster](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/clusters.html), a [Virtual Private Cloud (VPC)](https://aws.amazon.com/vpc/?vpc-blogs.sort-by=item.additionalFields.createdDate&vpc-blogs.sort-order=desc), a [Service](https://docs.aws.amazon.com/AmazonECS/latest/userguide/ecs_services.html), a [Task](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_definitions.html), and a Docker container. The container is created from the Image in the projects Container Registry The Service manages the Task (e.g., starts it, stops it, etc.). The VPC provides the networking infrastructure that the task/container runs in. And the cluster is the organizing unit for all of these artifacts. As mentioned before, each project must have its own set of these artifacts.

```plantuml
@startuml
cloud AWS {
    frame Cluster {
        storage VPC {
            storage Service {
                storage Task {
                    component Container {
                    }
                }
            }
        }
    }
}
cloud GitLab {
    frame Project {
        storage "Container Registry" as Registry1 {
            component Image as Image1
        }
        storage Repository as Repository1 {
            file ".gitlab-ci.yml" as gitlabci1
            file Dockerfile
        }
        storage "CI/CD Variables" as projectvars1 {
            artifact "AUTO_DEVOPS_PLATFORM_TARGET=FARGATE\nCI_AWS_ECS_CLUSTER=...\nCI_AWS_ECS_SERVICE=...\nCI_AWS_ECS_TASK_DEFINITION=..." as vars1
        }
    }
}
Dockerfile -> Image1
Image1 --> Container
@enduml
```

**_Figure 3: Structure of a GitLab Project and its AWS Cluster_**

## 2. Enable Review Apps

Most of this section can only be done by someone with access to AWS -- likely a Shop Manager or a Trustee.

### 2.1. Authorize GitLab to manage AWS ECS

1. Create a user that has `AmazonECS_FullAccess` IAM policy attached to it. Do this by...
   1. Create a group (e.g., `GitLabReviewApps`).
   2. Attach `AmazonECS_FullAccess` policy to the new group.
   3. Create a user (e.g., `GitLabReviewApps`) in that group with programmatic access.
      > IMPORTANT: Download the CSV file with the account's credentials. You cannot retrieve them later.
2. [Add AWS credentials to the LFP Group's CI/CD Variables in GitLab](https://about.gitlab.com/blog/2020/12/15/deploy-aws/) (Be sure to mask but not protect these variables).
   - `AWS_ACCESS_KEY_ID`
   - `AWS_DEFAULT_REGION`
   - `AWS_SECRET_ACCESS_KEY`

### 2.2. Add Dockerfile and .gitlab-ci.yml files to Project

1. Ensure there is a Dockerfile in the root of your project that is used to build the project's image.
2. Create `.gitlab-ci.yml` file with the following content:
   _[download the file](https://gitlab.com/LibreFoodPantry/common-services/review-apps/examples/single-container-project/-/raw/main/.gitlab-ci.yml)._
3. [Trigger a pipeline](https://docs.gitlab.com/ee/ci/pipelines/), which will create and push an initial image to your project's container registry.
   > NOTE: Only the `build` job will succeed. The rest will fail. That's OK.

### 2.3. Create Project's AWS ECS Artifacts

Each project needs an _AWS ECS Cluster_, _VPC_, _Service_, and _Task_ all backed by [Fargate](https://aws.amazon.com/fargate/?whats-new-cards.sort-by=item.additionalFields.postDateTime&whats-new-cards.sort-order=desc&fargate-blogs.sort-by=item.additionalFields.createdDate&fargate-blogs.sort-order=desc).

Each artifact (e.g., cluster, VPC, etc.) will need a name. The name needs to be unique across LFP. We recommend using the value of `CI_AWS_ECS_CLUSTER` in the `.gitlab-ci.yml` file. For example, ours is **librefoodpantry-common-services-review-apps-testing-dockerfile**. To find the value, look in the display_ip job for `CI_AWS_ECS_CLUSTER=${...}`.

To create these artifacts, sign into the [AWS Console](http://aws.amazon.com/) and select the Elastic Container Service (ECS). Then build each of the following in the order given below.

1. [Create a cluster](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/create_cluster.html) with the following configuration:
   - Name it using the name you constructed for all AWS artifacts.
   - Use "Networking only" (for Fargate)
   - Create a new VPC
2. [Create a task definition](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/create-task-definition.html) with the following options
   - Name it using the name you constructed for all AWS artifacts.
   - Uses Fargate
   - Uses `None` for the `Task Role`
   - With memory and CPU as small as is reasonable.
   - Add a container with the following options
     - Name it using the name you constructed for all AWS artifacts.
     - Has a URI that points to any one of the repository URIs in the project's Container Registry on GitLab.
         - Auto DevOps will update this Task definition with the specific URI for each commit. It just needs an initial one to begin with. If none yet exist, trigger a pipeline on the project and then check again after the build stage is done.
     - A hard or soft limit as small as is reasonable.
     - With ports that should be exposed (so far only tested with port 80)
3. [Create a service](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/create-service-console-v2.html) in the new cluster with the following options
   - Name it using the name you constructed for all AWS artifacts.
   - With the task definition and cluster you created above.
   - Using Fargate
   - Running 1 task
   - With any subnet (can be found on the next page of the service creator)

4. To test your configuration, open the running task and visit it's public IP in a browser (e.g., `http://public-ip/`).

### 2.4 Set CI variables for project

In your project's CI variables, define the following variables. There values should be the name that you created for AWS resource in the previous step. When creating, uncheck `protected` and do not mask.
   - `CI_AWS_ECS_CLUSTER`
   - `CI_AWS_ECS_SERVICE`
   - `CI_AWS_ECS_TASK_DEFINITION`

### 2.5 Test

1. Create a feature branch and a merge request in your project.
   - You should see a pipeline start running on your merge request.
   - And eventually a "View app" link should appear that lets you view your deployed review app.

## 3. Disable Review Apps

> Most of this section can only be done by someone with access to AWS -- likely a Shop Manager or a Trustee.

### 3.1. Disable Review Apps for a Project

1. [Delete the AWS ECS Cluster](https://docs.aws.amazon.com/AmazonECS/latest/userguide/delete_cluster.html) for the Project (TODO: Check that the VPC is deleted)
2. Remove `.gitlab-ci.yml` from Project

### 3.2. Disable Review Apps for **_all_** of LFP

1. Remove AWS ECS credentials from GitLab project's CI/CD variables.
2. To clean up, follow directions above for Disable Review Apps for a Project.
3. [Delete Review Apps Role from AWS](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_manage_delete.html)

## 4. User Instructions for Developer

### 4.1. Using with Merge Requests

1. Create a feature branch.
2. Make and commit some change to the feature branch.
3. Create a merge request and open the merge request tab after the review app has finished deploying.
4. Click the "View app" button in the pipeline section of the merge request.
5. Shut the app down through the merge request by clicking the "stop environment" button or wait until the app does so automatically.

_Note_: To speed up your CI/CD pipeline, [disable unused stages](https://docs.gitlab.com/ee/topics/autodevops/customize.html#disable-jobs) by adding CI/CD any of the following variables to the GitLab project:

   - `CODE_QUALITY_DISABLED=true`
   - `CONTAINER_SCANNING_DISABLED=true`
   - `SAST_DISABLED=true`
   - `LICENSE_MANAGEMENT_DISABLED=true`
   - `SECRET_DETECTION_DISABLED=true`
   - `TEST_DISABLED=true`

### 4.2 Using from Main

1. By default, review apps do not deploy from main; use feature branches and merge requests instead.

### 4.3. Limitations

1. This approach only works for a single review app for a single project.
