# Handoff: August 2021

## Authors

* Julia H.
* Colin B.
* Stoney J.

## The goal of this document

To provide those who come after us with enough information to pick up where we left off.

Beware, this document describes Review Apps current state at the end of Aug 2021. Things may have changed since then.

## The goal of the project

We want to allow developers (Shop Members) to use review apps to view their code in a sudo-production environment. The setup described allows review apps to access the credentials necessary to manipulate AWS without developers knowing the values of those credentials.

## Resources overview

Below is a high level map of the resources we used in GitLab.

- [Review Apps Group](https://gitlab.com/LibreFoodPantry/common-services/review-apps)
  - CI/CD variables: aws credentials are set as variables
  - [Wiki](https://gitlab.com/groups/LibreFoodPantry/common-services/review-apps/-/wikis/home) containing:
    - Bibliography of articles and documentation
    - Our Working Agreements
    - Iteration Summaries
  - [Epics](https://gitlab.com/groups/LibreFoodPantry/common-services/review-apps/-/epics)
  - Boards
    - [Epic Board](https://gitlab.com/groups/LibreFoodPantry/common-services/review-apps/-/epic_boards)
    - [Issue Current Iteration Board](https://gitlab.com/groups/LibreFoodPantry/common-services/review-apps/-/boards/2867777?iteration_id=Current&)
    - [Issue Iteration Planning Board](https://gitlab.com/groups/LibreFoodPantry/common-services/review-apps/-/boards/2867773)
  - [Examples Group](https://gitlab.com/LibreFoodPantry/common-services/review-apps/examples): For working examples
    - SingleContainerProject
        - CI/CD variables: disable Auto DevOps jobs that are unneccesary for debugging
  - [Playground Group](https://gitlab.com/LibreFoodPantry/common-services/review-apps/playground): Experimental code
  - Projects directly under the Review Apps group contain production code.

## ECS Implementation

This is the current production implementation. See 
the [documentation](https://gitlab.com/LibreFoodPantry/common-services/review-apps/documentation/) for a complete description and instructions for use.

[SingleContainerProject](https://gitlab.com/LibreFoodPantry/common-services/review-apps/examples/single-container-project) demonstrates how to use Review Apps to deploy to Amazon Web Services (AWS) using its Elastic Container Service (ECS) backed by Fargate.

Strengths

* Allows projects to make full use of GitLab's Auto DevOps.

Limitations

* Only one review app per project can be deployed.
* Each application must be a single container project with a Dockerfile in the root of the project.


## EKS Implementation

We were in the middle of developing this system when we had to stop development in Aug 2021.

Ideally this implementation would allow you to deploy multiple review apps instances into an EKS cluster and each application can have multiple containers via Docker Compose or Kubernetes (i.e., microservices).

We have been working towards it in our [playground](https://gitlab.com/LibreFoodPantry/common-services/review-apps/playground) group. Within this group there are the following projects:

- [eks-config](https://gitlab.com/LibreFoodPantry/common-services/review-apps/playground/eks-config): has scripts that tell the developer how to provision and deprovision resources necessary for EKS
- [eks-image](https://gitlab.com/LibreFoodPantry/common-services/review-apps/playground/eks-image): able to be used as an image in a .gitlab-ci.yml file. It provides a number of command-line tools for accessing and managing an EKS (e.g., AWS CLI, kubectl, and eksctl).
- [yelb](https://gitlab.com/LibreFoodPantry/common-services/review-apps/playground/yelb): example project that is capable of being deployed to EKS resources. This project contains the following branches:
  - yelb/[main](https://gitlab.com/LibreFoodPantry/common-services/review-apps/playground/yelb/-/tree/main): is a forked version of the example project from https://github.com/mreferre/yelb
  - yelb/[deploy](https://gitlab.com/LibreFoodPantry/common-services/review-apps/playground/yelb/-/tree/deploy): contains our edits to the example project to deploy to our EKS cluster using Gitlab CI. More details will be described below.

### Provision and deprovision an EKS cluster

See [eks-config](https://gitlab.com/LibreFoodPantry/common-services/review-apps/playground/eks-config)

Strengths

* Single command to provision an EKS.

Limitations

* The credentials that are used to provision the EKS must be used to deprovision the EKS.
* Currently must be cloned locally (i.e., does not run in a CI/CD pipeline).

### Deploy Yelb to EKS using Gitlab CI/CD

Usage

- In the yelb project on gitlab, the deploy branch has a [.gitlab-ci.yml](https://gitlab.com/LibreFoodPantry/common-services/review-apps/playground/yelb/-/blob/deploy/.gitlab-ci.yml) file that automatically deploys yelb to an existing EKS cluster when the CI/CD pipeline is run
  - To access the deployment, look into the logs of the review-deployment job and find the external-ip of the yelb-ui service (The section of the external-ip that says [MASKED] is actually us-east-2)
  - Copy the external ip (replacing [MASKED] with its actual value) and paste it into a new browser window to view the deployment
- To teardown the deployment, you need to manually press the play button on the teardown-deployment job
  - Although the job fails, everything in the deployment is properly torn down

Strengths

- Deploys a multi-container application using Kubernetes
- Docker Compose can be used with the same system by converting compose files to Kubernetes files using Kompose.

Limitations

- Deploys a single instance of yelb to EKS (want multiple)


### Manual Deployment of Yelb to EKS

During development, it is helpful to know how to manually deploy to EKS. This makes it faster and easier to try different technologies and concepts. This section describes how to manually deploy Yelb to EKS.

- Clone yelb to your local machine
- cd into the yelb project
- Install the following tools into your local environment
    - AWS-CLI
    - eksctl
    - kubectl
    - aws-iam-authenticator (ultimately may not be needed)
- Set your aws credentials in environmet variables (the same credentials that were used to provision the EKS cluster)
    - AWS_ACCESS_KEY_ID
    - AWS_SECRET_ACCESS_KEY
    - AWS_DEFAULT_REGION
- Generate and set the context kubectl will use to deploy to the EKS cluster
    ```
    aws eks update-kubeconfig --name <cluster-name> --kubeconfig <path-to-kubernetes-configuration-file>
    ```
- Change to the director that contains Yelbs Kubernetes configuration
    ```
    cd ./deployments/platformdeployment/Kubernetes/yaml
    ```
- Deploy Yelb to the EKS cluster
    ```
    kubectl apply -f yelb-k8s-ingress-alb-ip.yaml -f yelb-k8s-ingress-alb.yaml -f yelb-k8s-loadbalancer.yaml
    ```
- To view the deployment, use the following command to lookup the External IP for the load balancer and paste it into a browser.
    ```
    kubectl get services
    ```
-  To teardown the deployment
    ```
    kubectl delete -f yelb-k8s-loadbalancer.yaml -f yelb-k8s-ingress-alb.yaml -f yelb-k8s-ingress-alb-ip.yaml
    ```

### What needs to be solved to finish the EKS implementation

- Multiple instances need to run in a single EKS without interfering with eachother. Below are two technologies/concepts that may help solve this problem.
  - Kubernetes namespaces
  - Kubernetes host names
- We need to route to different instances running in the same EKS. Below are the technologies/concepts that may help solve this problem.
  - Kubernetes namespaces
  - Kubernetes host names
  - Kubernetes ingress service
  - Wildcard DNS A entry
- Set up a job in a .gitlab-ci.yml file that takes the external-IP of the deployment and saves it to a .env file
  - Create the namespace or hostname using a unique name for the instance that is constructed from the GitLab project slug and branch name.
  - Construct or extract the URL to the deployed instance and set the url for the GitLab environment to enable the "view app" link in merge requests.
